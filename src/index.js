import ReactDOM from 'react-dom'
import React from 'react'
import './index.css'
import Content from './pages/Content/Content'

class App extends React.Component{
    state = {
        page: 1,
    }
    updateState = (value) => {
        this.setState({ page: value })
    }

    render(){
        const {page} = this.state
        return(
            <div className='wrapper'>
                <ul className='pagination'>
                {[1,2,3].map((el, index) => 
                    <li
                        key={el}
                        onClick={() => {this.setState({page: el})}} 
                        className={`paginationItem ${page === el ? 'active' : ''}`}
                    />
                )}
                </ul>
                <Content updateState={this.updateState} num={this.state.page}/>
                <div className={`arrow ${page === 3 && 'arrowHide'}`} onClick={() => {this.setState({page: this.state.page+1})}}>
                    <div>Листайте вниз</div>
                    <div className='goDownIcon'></div>
                    <div className='downShadow'></div>
                </div>
            </div>
        )
    }
}
ReactDOM.render(<App/>, document.getElementById('root'))