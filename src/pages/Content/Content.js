import React from 'react'
import './Content.css'


class Content extends React.Component{
    start = 0;
    end = 0;
    sectionTwoWrap = React.createRef();
    sectionThreeWrap = React.createRef();
    touchStart = (event) => {
        this.start = event.changedTouches[0].clientY
        event.currentTarget.addEventListener('touchend', this.endTouch)
    }
    endTouch = (event) => {
        this.end = event.changedTouches[0].clientY
        const direction = this.start < this.end ? -1 : 1;
        this.props.updateState(this.props.num + direction);
    }
    currentPosition = (prevPageNum) => {
        const {num} = this.props;
        if(prevPageNum === 1 && num === 2){
            this.sectionTwoWrap.current.style.animation = 'fromPage 2s linear'
        } else if(prevPageNum === 2 && num === 1){
            this.sectionTwoWrap.current.style.animation = 'toPage 2s linear'
        } else if(prevPageNum === 2 && num === 3){
            this.sectionThreeWrap.current.style.animation = 'fromPage 2.5s linear'
            this.sectionTwoWrap.current.style.animation = 'toPageThree 2s linear'
        } else if(prevPageNum === 3 && num === 2){
            this.sectionThreeWrap.current.style.animation = 'toPage 2.5s linear'
            this.sectionTwoWrap.current.style.animation = 'fromPageThree 2s linear'
        } 
    }
    
    componentDidUpdate({ num }) {
        if (num !== this.props.num) {
            this.currentPosition(num);
        }
    }
    render(){
        return(
            <div className={`content page${this.props.num}`} onTouchStart={this.touchStart}>
                <section className='sectionOne'>
                    <h2>Всегда ли цель терапии СД2 <br/> на поверхности?</h2>
                    <div className='hypoglycemia'>
                        <p>Гипогликемия</p>
                        <div className='round round2'>
                            <div className='ellipse'></div>
                            <div className='animateEllipse'></div>
                            <div className='animateEllipseOne'></div>
                        </div>
                    </div>
                    <div className='complications'>
                        <p>Осложнения СД</p>
                        <div className='round round3'>
                            <div className='ellipse'></div>
                            <div className='animateEllipse'></div>
                            <div className='animateEllipseOne'></div>
                        </div>
                    </div>
                    <div className='HbA1c'>
                        <div className='round round1'>
                            <div className='ellipse'></div>
                            <div className='animateEllipse'></div>
                            <div className='animateEllipseOne'></div>
                        </div>
                        <p>Цель по HbA1c</p>
                    </div>
                    <div className='risks'>
                        <p>СС риски</p>
                        <div className='round round3'>
                            <div className='ellipse'></div>
                            <div className='animateEllipse'></div>
                            <div className='animateEllipseOne'></div>
                        </div>
                    </div>
                </section>
                <section className='sectionTwo'>
                    <div className='wrapperBg' ref={this.sectionTwoWrap}>
                        <h2>Основа терапии - <br/> патогенез СД2</h2>
                    </div>
                </section>
                <section className='sectionThree'>
                    <div className='wrapperBg' ref={this.sectionThreeWrap}>
                        <h3>Звенья патогенеза СД2</h3>
                        <div></div>
                    </div>
                </section>
            </div>
        )
    }
}

export default Content